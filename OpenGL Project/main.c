#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <string.h>
#include <time.h>

//Global Variables
#define BASE_HEIGHT 4.0/2
#define BASE_RADIUS 1.0/2
#define HEAD_HEIGHT 1.25/2
#define HEAD_RADIUS 0.75/2
#define NECK_HEIGHT 0.5/2
#define EYE_LEVEL 0.75/2
#define NOSE_LENGTH 0.5/2
#define LOWER_ARM_HEIGHT 2.0/2
#define LOWER_ARM_WIDTH 0.5/2
#define UPPER_ARM_HEIGHT 1.25/2
#define UPPER_ARM_WIDTH 0.5/2
#define ARM_TRANSLATION 0.22/2
#define alpha 0.0
#define pi 3.14159265
static GLfloat theta[] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
static GLint axis = 0;
GLUquadricObj *p;
GLfloat bound = 20.0; //For boundary
GLfloat camYpos = 3.0; //For camera height
GLfloat camXpos = 0.0; //For camera angle
GLfloat camZpos = 25; //For camera movement 
GLfloat x = 0.0;
GLfloat y = 0.0;
GLfloat xpos = 0.0;
GLfloat ypos = 0.0;
GLfloat zpos = 0.0;
GLfloat ambient[3];
GLfloat diffuse[3];
GLfloat specular[3];
GLfloat shiness[] = { 50.0f };

float width = 800;
float height = 800;
float temptInt = 0.0;

char printTimer[12];
int minutes = 0;
int seconds = 0;

struct Ball {
	GLfloat x, y, z;
};

struct Cube {
	GLfloat x, y, z;
};

struct Teapot {
	GLfloat x, y, z;
};

struct Cone {
	GLfloat x, y, z;
};

struct Cube cubes[4];
struct Ball balls[3];
struct Teapot teaPots[2];
struct Cone cones[2];

struct Cube makeCube(void);
struct Ball makeBall(void);
struct Teapot makeTeaPot(void);
struct Cone makeCone(void);

void displayCube(struct Cube);
void displayBall(struct Ball);
void displayTeaPot(struct Teapot);
void displayCone(struct Cone);

void sensor(struct Cube*, struct Ball*, struct Teapot*, struct Cone*);

void base(void);
void head(void);
void neck(void);
void upper_rarm(void);
void upper_larm(void);
void lower_rarm(void);
void lower_larm(void);

void init(void);
void displayFull(void);
void autoPosition(void);
void reshape(int width, int height);
void keyboard(unsigned char, int, int);
void processSpecialKeys(int, int, int);
void init1(void);
float randNum(void);
void displayText(void);

void base(void) {
	double angle, angleInc;
	int i;
	angle = pi / 180;
	angleInc = angle;
	glPushMatrix();

	ambient[0] = 1.0; ambient[1] = 0.0; ambient[2] = 0.0;
	diffuse[0] = 1.0; diffuse[1] = 0.0; diffuse[2] = 0.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);

	glRotatef(-90.0, 1.0, 0.0, 0.0);
	gluQuadricDrawStyle(p, GLU_FILL);
	gluCylinder(p, BASE_RADIUS, BASE_RADIUS, BASE_HEIGHT, 20, 20);

	glPopMatrix();

	glPushMatrix();

	gluQuadricDrawStyle(p, GLU_FILL);
	glTranslatef(0.0, BASE_HEIGHT, 0.0);
	glRotatef(-90.0, 1.0, 0.0, 0.0);
	gluDisk(p, 0.0, BASE_RADIUS, 20, 20);
	glTranslatef(0.0, 0.0, -BASE_HEIGHT);
	gluDisk(p, 0.0, BASE_RADIUS, 20, 20);
	glPopMatrix();
}

void neck(void) {
	glPushMatrix();

	ambient[0] = 1.0; ambient[1] = 1.0; ambient[2] = 0.0;
	diffuse[0] = 1.0; diffuse[1] = 1.0; diffuse[2] = 0.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);

	glTranslatef(0.0, BASE_HEIGHT, 0.0);
	glRotatef(-90.0, 1.0, 0.0, 0.0);
	gluQuadricDrawStyle(p, GLU_FILL);
	gluCylinder(p, HEAD_RADIUS / 2, HEAD_RADIUS / 2, HEAD_HEIGHT, 8, 6);
	glPopMatrix();
}

void head(void) {
	glPushMatrix();

	ambient[0] = 1.0; ambient[1] = 0.0; ambient[2] = 1.0;
	diffuse[0] = 1.0; diffuse[1] = 0.0; diffuse[2] = 1.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);

	glRotatef(-90.0, 1.0, 0.0, 0.0);
	gluQuadricDrawStyle(p, GLU_FILL);
	gluCylinder(p, HEAD_RADIUS, HEAD_RADIUS, HEAD_HEIGHT, 20, 20);

	glPushMatrix();

	gluDisk(p, 0.0, HEAD_RADIUS, 20, 20);
	glTranslatef(0.0, 0.0, HEAD_HEIGHT);
	gluDisk(p, 0.0, HEAD_RADIUS, 20, 20);
	glPopMatrix();

	//Left Eye
	glPushMatrix();
	glTranslatef(0.25, -HEAD_RADIUS + 0.12, EYE_LEVEL);
	ambient[0] = 1.0; ambient[1] = 1.0; ambient[2] = 1.0;
	diffuse[0] = 1.0; diffuse[1] = 1.0; diffuse[2] = 1.0;
	specular[0] = 0.5; specular[1] = 0.5; specular[2] = 0.5;
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);
	gluQuadricDrawStyle(p, GLU_FILL);
	gluSphere(p, 0.125, 6, 6);
	glPopMatrix();

	//Right Eye
	glPushMatrix();
	glTranslatef(-0.25, -HEAD_RADIUS + 0.12, EYE_LEVEL);
	ambient[0] = 1.0; ambient[1] = 1.0; ambient[2] = 1.0;
	diffuse[0] = 1.0; diffuse[1] = 1.0; diffuse[2] = 1.0;
	specular[0] = 0.5; specular[1] = 0.5; specular[2] = 0.5;
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);
	gluQuadricDrawStyle(p, GLU_FILL);
	gluSphere(p, 0.125, 6, 6);
	glPopMatrix();

	//Nose
	glPushMatrix();
	ambient[0] = 1.0; ambient[1] = 0.5; ambient[2] = 0.0;
	diffuse[0] = 1.0; diffuse[1] = 0.5; diffuse[2] = 0.0;
	specular[0] = 0.5; specular[1] = 0.5; specular[2] = 0.5;
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);
	glTranslatef(0.0, -HEAD_RADIUS, NOSE_LENGTH);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	gluQuadricDrawStyle(p, GLU_FILL);
	gluCylinder(p, 0.125, 0, NOSE_LENGTH, 8, 6);
	glPopMatrix();

	glPopMatrix();
}

void lower_rarm(void) {
	glPushMatrix();

	ambient[0] = 0.0; ambient[1] = 1.0; ambient[2] = 0.0;
	diffuse[0] = 0.0; diffuse[1] = 1.0; diffuse[2] = 0.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);

	glTranslatef(0.0, 0.5 * LOWER_ARM_HEIGHT, ARM_TRANSLATION);
	glScalef(LOWER_ARM_WIDTH, LOWER_ARM_HEIGHT, LOWER_ARM_WIDTH);
	glutSolidCube(1.0);
	glPopMatrix();
}

void lower_larm(void) {
	glPushMatrix();

	ambient[0] = 0.0; ambient[1] = 1.0; ambient[2] = 0.0;
	diffuse[0] = 0.0; diffuse[1] = 1.0; diffuse[2] = 0.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);

	glTranslatef(0.0, 0.5 * LOWER_ARM_HEIGHT, -ARM_TRANSLATION);
	glScalef(LOWER_ARM_WIDTH, LOWER_ARM_HEIGHT, LOWER_ARM_WIDTH);
	glutSolidCube(1.0);
	glPopMatrix();
}

void upper_rarm(void) {
	glPushMatrix();

	ambient[0] = 0.0; ambient[1] = 0.0; ambient[2] = 1.0;
	diffuse[0] = 0.0; diffuse[1] = 0.0; diffuse[2] = 1.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);

	glTranslatef(0.0, 0.5 * UPPER_ARM_HEIGHT, ARM_TRANSLATION);
	glScalef(UPPER_ARM_WIDTH, UPPER_ARM_HEIGHT, UPPER_ARM_WIDTH);
	glutSolidCube(1.0);
	glPopMatrix();
}

void upper_larm(void) {
	glPushMatrix();

	ambient[0] = 0.0; ambient[1] = 0.0; ambient[2] = 1.0;
	diffuse[0] = 0.0; diffuse[1] = 0.0; diffuse[2] = 1.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);

	glTranslatef(0.0, 0.5 * UPPER_ARM_HEIGHT, -ARM_TRANSLATION);
	glScalef(UPPER_ARM_WIDTH, UPPER_ARM_HEIGHT, UPPER_ARM_WIDTH);
	glutSolidCube(1.0);
	glPopMatrix();
}

void displayHotBod(void) {
	//FULL BODY
	glTranslatef(xpos, ypos, zpos);
	glRotatef(theta[0], 0.0, 1.0, 0.0);
	base();
	neck();

	glPushMatrix();
	glTranslatef(0.0, BASE_HEIGHT + HEAD_HEIGHT / 2, 0.0);
	glRotatef(theta[2], 1.0, 0.0, 0.0);
	glRotatef(theta[1], 0.0, 1.0, 0.0);
	head();
	glPopMatrix();

	//Right Arm
	glPushMatrix();
	glTranslatef(BASE_RADIUS, BASE_HEIGHT - BASE_RADIUS / 2, 0.0);
	glRotatef(180.0, 0.0, 0.0, 1.0);
	glRotatef(270.0, 0.0, 1.0, 0.0);
	glRotatef(theta[4], 0.0, 0.0, 1.0);
	lower_rarm();
	glTranslatef(0.0, LOWER_ARM_HEIGHT, 0.0);
	glRotatef(0.0, 0.0, 0.0, 180.0);
	glRotatef(theta[6], 0.0, 0.0, 1.0);
	upper_rarm();
	glPopMatrix();

	//Left Arm
	glPushMatrix();
	glTranslatef(-BASE_RADIUS, BASE_HEIGHT - BASE_RADIUS / 2, 0.0);
	glRotatef(180.0, 0.0, 0.0, 1.0);
	glRotatef(270.0, 0.0, 1.0, 0.0);
	glRotatef(theta[3], 0.0, 0.0, 1.0);
	lower_larm();
	glTranslatef(0.0, LOWER_ARM_HEIGHT, 0.0);
	glRotatef(0.0, 0.0, 0.0, 180.0);
	glRotatef(theta[5], 0.0, 0.0, 1.0);
	upper_larm();
	glPopMatrix();
}

int randInt = 0;

void displayFull(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	//Camera Angle

	gluLookAt(camXpos, camYpos, camZpos, 1.5, 0, 0.0, 0.0, 1.0, 0.0);

	//Earth is flat
	glPushMatrix();
	ambient[0] = 0.5; ambient[1] = 0.5; ambient[2] = 0.5;
	diffuse[0] = 1.0; diffuse[1] = 1.0; diffuse[2] = 1.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);
	glBegin(GL_POLYGON);
	glVertex3f(bound, 0.0, bound);
	glVertex3f(bound, 0.0, -bound);
	glVertex3f(-bound, 0.0, -bound);
	glVertex3f(-bound, 0.0, bound);
	glEnd();
	glPopMatrix();

	//Cube
	displayCube(cubes[0]);
	displayCube(cubes[1]);
	displayCube(cubes[2]);
	displayCube(cubes[3]);

	//every 13 second
	if (seconds % 13 == 0 & temptInt == 0.0) {
		randInt = rand() % 4;
		cubes[randInt] = makeCube();
	}

	//Ball
	displayBall(balls[0]);
	displayBall(balls[1]);
	displayBall(balls[2]);

	if (seconds % 20 == 0 & temptInt == 0.0) {
		randInt = rand() % 3;
		balls[randInt] = makeBall();
	}

	//Cone
	displayCone(cones[0]);
	displayCone(cones[1]);

	if (seconds % 40 == 0 & temptInt == 0.0) {
		randInt = rand() % 2;
		cones[randInt] = makeCone();

	}

	//Teapot
	displayTeaPot(teaPots[0]);
	displayTeaPot(teaPots[1]);

	if (seconds % 30 == 0 & temptInt == 0.0) {
		randInt = rand() % 2;
		teaPots[randInt] = makeTeaPot();

	}

	glPushMatrix();
	displayHotBod();
	glPopMatrix();

	displayText();

	autoPosition();

	Sleep(105);

	glFlush();

	glutSwapBuffers();
	glutPostRedisplay();

}

void displayText(void) {

	// timer
	temptInt += 0.01;
	if (temptInt >= 0.08) {
		seconds++;
		temptInt = 0.0;
		if (seconds == 60) {
			minutes++;
			seconds = 0;
		}
	}

	// int to string
	snprintf(printTimer, sizeof(printTimer) - 1, "%02d : %02d", minutes, seconds);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	gluOrtho2D(0, width, 0, height);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glRasterPos2f(10, 780);
	glColor3f(1.0, 1.0, 1.0);

	glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, printTimer); //print

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

}

void moveForward(void) {
	xpos -= cos(90 * pi / 180 + theta[0] * pi / 180);
	zpos += sin(90 * pi / 180 + theta[0] * pi / 180);
}

void turnSlightLeft(void) {
	theta[0] += 45.0;
}

void turnSlightRight(void) {
	theta[0] -= 45.0;
}

void autoPosition(void) {
	int rng = rand() % 2;
	static int wallCount;
	moveForward();

	if (seconds % 2 == 0 && temptInt == 0.00) {
		if (rng == 0) {
			turnSlightLeft();
		}
		else if (rng == 1) {
			turnSlightRight();
		}
	}

	if (xpos > bound) {
		xpos = bound;
		wallCount++;
		printf("FOUND WALL(%02d:%02d) Wall Collision: %d\n", minutes, seconds, wallCount);
		theta[0] -= 180.0;
		if (rng == 0) {
			turnSlightLeft();
		}
		if (rng == 1) {
			turnSlightRight();
		}
	}
	else if (xpos < -bound) {
		xpos = -bound;
		wallCount++;
		printf("FOUND WALL(%02d:%02d) Wall Collision: %d\n", minutes, seconds, wallCount);
		theta[0] += 180.0;
		if (rng == 0) {
			turnSlightLeft();
		}
		if (rng == 1) {
			turnSlightRight();
		}
	}

	if (zpos > bound) {
		zpos = bound;
		theta[0] -= 180.0;
		wallCount++;
		printf("FOUND WALL(%02d:%02d) Wall Collision: %d\n", minutes, seconds, wallCount);
		if (rng == 0) {
			turnSlightLeft();
		}
		if (rng == 1) {
			turnSlightRight();
		}
	}
	else if (zpos < -bound) {
		zpos = -bound;
		theta[0] += 180.0;
		wallCount++;
		printf("FOUND WALL(%02d:%02d) Wall Collision: %d\n", minutes, seconds, wallCount);
		if (rng == 0) {
			turnSlightLeft();
		}
		if (rng == 1) {
			turnSlightRight();
		}
	}
	sensor(cubes, balls, teaPots, cones);
}

void sensor(struct Cube* cubes, struct Ball* balls, struct Teapot* teapots, struct Cone* cones) {
	int cubeSize, ballSize, teapotSize, coneSize;
	int i;
	int randInt = rand() % 2;
	static int cubeCount, ballCount, teapotCount, coneCount;

	cubeSize = 4;
	ballSize = 3;
	teapotSize = 2;
	coneSize = 2;

	for (i = 0; i < cubeSize; i++) {
		if (xpos >= (cubes[i].x - 2) && xpos <= (cubes[i].x + 2)) {
			if (zpos >= (cubes[i].z - 2) && zpos <= (cubes[i].z + 2)) {
				theta[0] -= 180.0;
				cubeCount++;
				printf("FOUND CUBE(%02d:%02d) Cube Collision: %d\n", minutes, seconds, cubeCount);
				if (randInt == 0) {
					turnSlightLeft();
				}
				else {
					turnSlightRight();
				}
			}

		}
	}

	for (i = 0; i < ballSize; i++) {
		if (xpos >= (balls[i].x - 2) && xpos <= (balls[i].x + 2)) {
			if (zpos >= (balls[i].z - 2) && zpos <= (balls[i].z + 2)) {
				theta[0] -= 180.0;
				ballCount++;
				printf("FOUND BALL(%02d:%02d) Ball Collision: %d\n", minutes, seconds, ballCount);
				if (randInt == 0) {
					turnSlightLeft();
				}
				else {
					turnSlightRight();
				}

			}

		}
	}

	for (i = 0; i < teapotSize; i++) {
		if (xpos >= (teapots[i].x - 2) && xpos <= (teapots[i].x + 2)) {
			if (zpos >= (teapots[i].z - 2) && zpos <= (teapots[i].z + 2)) {
				theta[0] -= 180.0;
				teapotCount++;
				printf("FOUND TEAPOT(%02d:%02d) Teapot Collision: %d\n", minutes, seconds, teapotCount);
				if (randInt == 0) {
					turnSlightLeft();
				}
				else {
					turnSlightRight();
				}
			}

		}
	}

	for (i = 0; i < coneSize; i++) {
		if (xpos >= (cones[i].x - 2) && xpos <= (cones[i].x + 2)) {
			if (zpos >= (cones[i].z - 2) && zpos <= (cones[i].z + 2)) {
				theta[0] -= 180.0;
				coneCount++;
				turnSlightLeft();
				printf("FOUND CONE(%02d:%02d) Cone Collision: %d\n", minutes, seconds, coneCount);
				if (randInt == 0) {
					turnSlightLeft();
				}
				else {
					turnSlightRight();
				}
			}

		}
	}
}

void keyboardCam(unsigned char key, int x, int y) {
	switch (key) {
	case 'w':
		if (camYpos < bound) {
			camYpos += 1.5;
		}
		break;

	case 's':
		if (camYpos > 2) {
			camYpos -= 1.5;
		}
		break;

	case 'a':
		if (camXpos > -bound * 3)
			camXpos -= 1.5;
		break;

	case 'd':
		if (camXpos < bound * 3)
			camXpos += 1.5;
		break;

	case 'i':
		if (camZpos > -bound * 3)
			camZpos -= 1.5;
		break;

	case 'o':
		if (camZpos < bound * 3)
			camZpos += 1.5;
		break;
	}
}

void reshape(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(100.0, (GLfloat)w / (GLfloat)h, 0.5, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glClearColor(1.0f, 1.0f, 1.0f, 0);
	glLoadIdentity();
}

void init(void) {
	GLfloat lightIntensity[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	GLfloat light_position[] = { 2.0f, 6.0f, 3.0f, 0.0f };

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);

	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightIntensity);

	p = gluNewQuadric();
}

void init1(void) {
	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_shininess[] = { 50.0 };
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat white_light[] = { 0.3, 0.3, 0.3, 1.0 };
	GLfloat lmodel_ambient[] = { 1.0, 1.0, 0.0, 1.0 };

	glShadeModel(GL_SMOOTH);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light);
	glLightfv(GL_LIGHT0, GL_SPECULAR, white_light);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
}

int main(int argc, char **argv) {

	cubes[0] = makeCube();
	cubes[1] = makeCube();
	cubes[2] = makeCube();
	cubes[3] = makeCube();

	balls[0] = makeBall();
	balls[1] = makeBall();
	balls[2] = makeBall();

	teaPots[0] = makeTeaPot();
	teaPots[1] = makeTeaPot();

	cones[0] = makeCone();
	cones[1] = makeCone();

	srand(time(0));
	glutInit(&argc, argv);
	srand((unsigned int)time(NULL)); //for rand() rng
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutCreateWindow("Robot");
	init();
	init1();
	glutReshapeFunc(reshape);
	glutDisplayFunc(displayFull);
	glutKeyboardFunc(keyboardCam);

	glutMainLoop();

	return 0;
}

float randNum(void) {
	float random;
	const int MAX = bound;
	random = rand() % MAX;
	random *= pow(-1, random);

	return random;
}

void displayCube(struct Cube disCube) {
	glPushMatrix();
	ambient[0] = 0.3; ambient[1] = 0.3; ambient[2] = 0.3;
	diffuse[0] = 0.0; diffuse[1] = 0.0; diffuse[2] = 1.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);
	glTranslatef(disCube.x, disCube.y, disCube.z); //xyz position
	glutSolidCube(2.0);
	glPopMatrix();
}

void displayBall(struct Ball disBall) {
	glPushMatrix();
	ambient[0] = 1.0; ambient[1] = 0.3; ambient[2] = 0.3;
	diffuse[0] = 1.0; diffuse[1] = 1.0; diffuse[2] = 1.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);
	glTranslatef(disBall.x, disBall.y, disBall.z); //xyz position
	glutSolidSphere(1.0, 10, 100);
	glPopMatrix();
}

void displayTeaPot(struct Teapot disPot) {
	glPushMatrix();
	ambient[0] = 0.3; ambient[1] = 0.3; ambient[2] = 0.3;
	diffuse[0] = 1.0; diffuse[1] = 0.0; diffuse[2] = 0.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);
	glTranslatef(disPot.x, disPot.y, disPot.z); //xyz position
	glutSolidTeapot(1.0);
	glPopMatrix();
}

void displayCone(struct Cone disCone) {
	glPushMatrix();
	ambient[0] = 0.3; ambient[1] = 0.3; ambient[2] = 0.3;
	diffuse[0] = 1.0; diffuse[1] = 0.0; diffuse[2] = 1.0;
	specular[0] = 0.7; specular[1] = 0.6; specular[2] = 0.5;
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shiness);
	glTranslatef(disCone.x, disCone.y, disCone.z); //xyz position
	glutSolidCone(0.5, 1.5, 20, 15);
	glPopMatrix();
}

struct Cube makeCube() {
	struct Cube theCube;
	theCube.x = randNum();
	theCube.y = 1.0;
	theCube.z = randNum();
	return theCube;
}

struct Ball makeBall() {
	struct Ball theBall;
	theBall.x = randNum();
	theBall.y = 1.0;
	theBall.z = randNum();
	return theBall;
}

struct Teapot makeTeaPot() {
	struct Teapot thePot;
	thePot.x = randNum();
	thePot.y = 1.0;
	thePot.z = randNum();
	return thePot;
}

struct Cone makeCone() {
	struct Cone theCone;
	theCone.x = randNum();
	theCone.y = 1.0;
	theCone.z = randNum();
	return theCone;
}